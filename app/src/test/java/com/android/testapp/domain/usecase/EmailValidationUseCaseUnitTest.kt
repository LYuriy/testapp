package com.android.testapp.domain.usecase

import org.junit.Assert.assertEquals
import org.junit.Test

class EmailValidationUseCaseUnitTest {

    private var emailValidationUseCase = EmailValidationUseCase()

    @Test
    fun testCorrectEmailValidation() {
        with(emailValidationUseCase) {
            assertEquals("Email validation failed", true, validate("a@b.co"))
            assertEquals("Email validation failed", true, validate("a1@google.com"))
        }
    }

    @Test
    fun testIncorrectEmailValidation() {
        with(emailValidationUseCase) {
            assertEquals("Email validation failed", false, validate("@b.co"))
            assertEquals("Email validation failed", false, validate("a1@googlecom"))
            assertEquals("Email validation failed", false, validate("a1@"))
            assertEquals("Email validation failed", false, validate(""))
            assertEquals("Email validation failed", false, validate("a1google.com"))
        }
    }
}