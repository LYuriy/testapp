package com.android.testapp.data.repository

import com.android.testapp.data.db.UserDao
import com.android.testapp.data.model.UserRoomEntity
import com.android.testapp.domain.entity.RegisterStatus
import com.android.testapp.domain.entity.Status
import com.android.testapp.domain.entity.User
import com.android.testapp.domain.repository.UserRepository
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(private val userDao: UserDao) : UserRepository {
    override suspend fun register(email: String, password: String): RegisterStatus {
        if(getUser(email) != null) return RegisterStatus(Status.Failure, "user with entered email already exists")
        userDao.insert(UserRoomEntity(-1, email, password))
        return RegisterStatus(Status.Success)

    }

    override suspend fun getUser(email: String): User? {
        return userDao.getUser(email)?.toDomainModel()
    }
}