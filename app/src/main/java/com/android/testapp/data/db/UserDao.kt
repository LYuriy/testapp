package com.android.testapp.data.db

import androidx.room.*
import com.android.testapp.data.model.UserRoomEntity

@Dao
interface UserDao {

    @Query("SELECT * FROM users WHERE email = :email")
    fun getUser(email: String): UserRoomEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(userRoomEntity: UserRoomEntity)

    @Update
    fun update(userRoomEntity: UserRoomEntity)

    @Delete
    fun delete(userRoomEntity: UserRoomEntity)
}
