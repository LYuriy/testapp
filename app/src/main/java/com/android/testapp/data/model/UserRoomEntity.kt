package com.android.testapp.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.android.testapp.domain.entity.User

@Entity(tableName = "users")
data class UserRoomEntity(
    @PrimaryKey(autoGenerate = true) val id: Long,
    val email: String,
    val password: String
) {
    fun toDomainModel(): User {
        return User(email)
    }
}