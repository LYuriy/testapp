package com.android.testapp.domain.entity

data class RegisterStatus(val status : Status, val reason: String? = null)
