package com.android.testapp.domain.usecase

import com.android.testapp.domain.entity.RegisterStatus
import com.android.testapp.domain.repository.UserRepository
import javax.inject.Inject

class RegisterUseCase @Inject constructor(private val userRepository: UserRepository) {

    suspend fun register(login: String, password: String): RegisterStatus {
        return userRepository.register(login, password)
    }
}