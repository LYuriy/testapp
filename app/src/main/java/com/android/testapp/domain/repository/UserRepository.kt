package com.android.testapp.domain.repository

import com.android.testapp.domain.entity.RegisterStatus
import com.android.testapp.domain.entity.User

interface UserRepository {
    suspend fun register(email: String, password: String) : RegisterStatus

    suspend fun getUser(email: String) : User?
}