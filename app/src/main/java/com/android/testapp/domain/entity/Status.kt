package com.android.testapp.domain.entity

enum class Status {
    Success,
    Failure
}