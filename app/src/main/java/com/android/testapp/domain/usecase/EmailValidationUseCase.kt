package com.android.testapp.domain.usecase

import java.util.regex.Pattern
import javax.inject.Inject

class EmailValidationUseCase @Inject constructor() {
    fun validate(email: String) : Boolean {
        if (email.isEmpty()) return false
        return EMAIL_ADDRESS.matcher(email).matches()
    }

    companion object {
        val EMAIL_ADDRESS = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
        )
    }
}