package com.android.testapp.presentation.register

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.testapp.domain.entity.RegisterStatus
import com.android.testapp.domain.entity.Status
import com.android.testapp.domain.usecase.EmailValidationUseCase
import com.android.testapp.domain.usecase.RegisterUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val registerUseCase : RegisterUseCase,
    private val emailValidationUseCase: EmailValidationUseCase
) : ViewModel() {
    lateinit var registerView: RegisterView

    fun register(login: String, password: String) {
        val job = viewModelScope.async(Dispatchers.IO) { registerUseCase.register(login, password) }
        viewModelScope.launch(Dispatchers.Main) { onResult(job.await()) }
    }

    private fun onResult(status : RegisterStatus) {
        when(status.status) {
            Status.Success -> registerView.navigateToSuccess()
            Status.Failure -> status.reason?.let { registerView.showError(status.reason)}
        }
    }

    fun isEmailValid(email: String): Boolean {
        return emailValidationUseCase.validate(email)
    }
}