package com.android.testapp.presentation.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.android.testapp.R
import com.android.testapp.databinding.RegisterFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegisterFragment : Fragment(), RegisterView {
    private lateinit var binding: RegisterFragmentBinding
    private val viewModel: RegisterViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = RegisterFragmentBinding.inflate(inflater, container, false)
        viewModel.registerView = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            registerButton.setOnClickListener {
                if (viewModel.isEmailValid(login.text.toString())) {
                    if (password.text.isNotEmpty()) {
                        viewModel.register(login.text.toString(), password.text.toString())
                    } else {
                        password.error = getString(R.string.error_empty_password)
                    }
                } else {
                    login.error = getString(R.string.error_invalid_email)
                }
            }
        }
    }

    override fun navigateToSuccess() {
        findNavController().navigate(
            R.id.action_registerFragment_to_successFragment
        )
    }

    override fun showError(reason: String) {
        Toast.makeText(context, reason, Toast.LENGTH_LONG).show()
    }
}