package com.android.testapp.presentation.register

import com.android.testapp.presentation.base.BaseView

interface RegisterView : BaseView {
    fun navigateToSuccess()
    fun showError(reason: String)
}