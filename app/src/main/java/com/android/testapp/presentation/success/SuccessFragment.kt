package com.android.testapp.presentation.success

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.android.testapp.databinding.SuccessFragmentBinding

class SuccessFragment : Fragment() {

    private lateinit var binding: SuccessFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = SuccessFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }
}