package com.android.testapp.core.di

import android.content.Context
import com.android.testapp.data.db.AppDatabase
import com.android.testapp.data.db.UserDao
import com.android.testapp.data.repository.UserRepositoryImpl
import com.android.testapp.domain.repository.UserRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) =
        AppDatabase.getDatabase(appContext)

    @Singleton
    @Provides
    fun provideUserDao(db: AppDatabase) = db.userDao()

    @Singleton
    @Provides
    fun provideUserRepository(
        userDao: UserDao
    ) : UserRepository {
        return UserRepositoryImpl(userDao)
    }
}